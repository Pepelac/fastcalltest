package com.PepelacStudio.fastcalltest;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import com.PepelacStudio.fastcalltest.database.Contact;
import com.PepelacStudio.fastcalltest.database.DatabaseHandler;


/**
 * Created by abantonx on 02.09.13.
 */



public class AppWidget extends AppWidgetProvider {
    public static String ACTION_WIDGET_UPDATE_FROM_ACTIVITY = "ACTION.WIDGET.UPDATE.FROM.ACTIVITY";
    public static String EXTRA_WORD="test.test.test.WORD";

    private DatabaseHandler db;
    private Contact ct;
    private static final int[] IMAGES={R.drawable.ic_launcher, R.drawable.ic_launcher};
    public static String ACTION_WIDGET_REFRESH = "ActionReceiverRefresh";
    @Override
    public void onUpdate(Context ctxt, AppWidgetManager mgr,
                         int[] appWidgetIds) {
      //  Log.i("gotcha","update");




        for (int i=0; i<appWidgetIds.length; i++) {
            Intent svcIntent=new Intent(ctxt, WidgetService.class);

            svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews widget=new RemoteViews(ctxt.getPackageName(),
                    R.layout.widget);

            widget.setRemoteAdapter(appWidgetIds[i], R.id.contacts,
                    svcIntent);

            Intent clickIntent=new Intent(ctxt, AppWidget.class);
  /*          PendingIntent clickPI=PendingIntent
                    .getActivity(ctxt, 0,
                            clickIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
*/

            clickIntent.setAction(ACTION_WIDGET_REFRESH);
            clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds[i]);

            PendingIntent pi=PendingIntent.getBroadcast(ctxt, 0 , clickIntent,  PendingIntent.FLAG_UPDATE_CURRENT);


            widget.setPendingIntentTemplate(R.id.contacts, pi);
            mgr.updateAppWidget(appWidgetIds[i], widget);

/*
            Intent intent = new Intent(ctxt, WidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    appWidgetIds[i]);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews rv = new RemoteViews(ctxt.getPackageName(),
                    R.layout.widget);

            rv.setRemoteAdapter(appWidgetIds[i], R.id.contacts, intent);

            rv.setEmptyView(R.id.contacts, R.id.empty_view);
            mgr.updateAppWidget(appWidgetIds[i], rv);
*/

        }


        super.onUpdate(ctxt, mgr, appWidgetIds);
    }/*
        ComponentName me=new ComponentName(ctxt, AppWidget.class);

        mgr.updateAppWidget(me, buildUpdate(ctxt, appWidgetIds));
    }*/
    @Override
        public void onReceive(Context context, Intent intent) {

        Log.i("gotcha","receive");

        Bundle extras = intent.getExtras();
        if (intent.getAction() != null){

            Log.i("receive action", intent.getAction());
        }
        if (!intent.getAction().equals(ACTION_WIDGET_UPDATE_FROM_ACTIVITY) &&
            !intent.getAction().equals("android.appwidget.action.APPWIDGET_ENABLED") &&
            !intent.getAction().equals("android.appwidget.action.APPWIDGET_DISABLED") &&
            !intent.getAction().equals("android.appwidget.action.APPWIDGET_DELETED") &&
            !intent.getAction().equals("android.appwidget.action.APPWIDGET_UPDATE")&&
            !intent.getAction().equals("android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"))
        {
            int extrass= -1;
            if(extras != null) {
                extrass = extras.getInt("position");

            }
            db = new DatabaseHandler(context);
            ct = new Contact();
            ct = db.Get_Contact(extrass+1);
            String phone ="no phone";
            if (ct != null) {
            Log.i("phone",ct.getPhoneNumber());
                phone = ct.getPhoneNumber();
            }

            Log.i("receive position", Integer.toString(extrass));
                if (extras.getString("phone")!=null){

                    phone = ct.getPhoneNumber();
                }
                ct=null;
                db=null;
            if (phone != "no phone" && phone != null ){
                String newPhone = phone.replaceAll("[^0-9+]","");

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                callIntent.setData(Uri.parse("tel:"+newPhone));
                context.startActivity(callIntent);
            }

        } else {
            super.onReceive(context, intent);
        }


        if(extras!=null) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), AppWidget.class.getName());
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.contacts);
            onUpdate(context, appWidgetManager, appWidgetIds);
        }
    }


/*
    private RemoteViews updateWidgetListView(Context context,
                                             int appWidgetId) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),R.layout.widget);
        Intent svcIntent = new Intent(context, WidgetService.class);
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        svcIntent.setData(Uri.parse(
                svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        //setting adapter to listview of the widget
        remoteViews.setRemoteAdapter(appWidgetId, R.id.contacts, svcIntent);
        //setting an empty view in case of no data
        remoteViews.setEmptyView(R.id.contacts, R.id.row);
        return remoteViews;
    }
*/
    private RemoteViews buildUpdate(Context ctxt, int[] appWidgetIds) {
        RemoteViews updateViews=new RemoteViews(ctxt.getPackageName(), R.layout.widget);

       Intent i=new Intent(ctxt, AppWidget.class);

        i.setAction(ACTION_WIDGET_REFRESH);
        i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

        PendingIntent pi=PendingIntent.getBroadcast(ctxt, 0 , i,
                PendingIntent.FLAG_UPDATE_CURRENT);

        updateViews.setOnClickPendingIntent(R.id.contacts, pi);

        updateViews.setOnClickPendingIntent(R.id.background, pi);

        return(updateViews);
    }


}