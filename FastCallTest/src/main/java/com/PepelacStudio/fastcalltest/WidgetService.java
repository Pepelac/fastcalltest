package com.PepelacStudio.fastcalltest;

/**
 * Created by abantonx on 03.09.13.
 */

        import android.content.Intent;
        import android.util.Log;
        import android.widget.RemoteViewsService;

        import com.PepelacStudio.fastcalltest.ViewFactory;

public class WidgetService extends RemoteViewsService {
    private String[] items;//= new String[]{"test1","test2","test3","test4"};
    public void setItems(String[] it){
        items=it;
    }

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {

        ViewFactory vf = new ViewFactory(this.getApplicationContext(),intent);

        vf.setItems(items);
        return(vf);

    }
}