package com.PepelacStudio.fastcalltest.database;

/**
 * Created by abantonx on 02.09.13.
 */

public class Contact {

    // private variables
    public int _id;
    public String _name;
    public String _phone_number;
    public byte[] _photo;

    public Contact() {
    }

    // constructor
    public Contact(int id, String name, String _phone_number,byte[] photo) {
        this._id = id;
        this._name = name;
        this._phone_number = _phone_number;
        this._photo = photo;
    }

    // constructor
    public Contact(String name, String _phone_number,byte[] photo) {
        this._name = name;
        this._phone_number = _phone_number;
        this._photo = photo;
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting name
    public String getName() {
        return this._name;
    }

    // setting name
    public void setName(String name) {
        this._name = name;
    }

    // getting phone number
    public String getPhoneNumber() {
        return this._phone_number;
    }

    // setting phone number
    public void setPhoneNumber(String phone_number) {
        this._phone_number = phone_number;
    }
    // getting photo
    public byte[] getPhoto() {
        return this._photo;
    }

    // setting photo
    public void setPhoto(byte[] photo) {
        this._photo = photo;
    }

}