package com.PepelacStudio.fastcalltest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;

import android.provider.ContactsContract;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.PepelacStudio.fastcalltest.database.Contact;
import com.PepelacStudio.fastcalltest.database.DatabaseHandler;
import com.google.analytics.tracking.android.EasyTracker;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {
    public static String ACTION_WIDGET_UPDATE_FROM_ACTIVITY = "ACTION.WIDGET.UPDATE.FROM.ACTIVITY";
    Button b;
    ListView lv ;
    Button addContact;
    TextView test;
    ScrollView scw;
    TextView tv;
    TextView tv2;
    private ArrayList<Item> m_parts = new ArrayList<Item>();
    DatabaseHandler db;
    Contact ct;
    private ListAdapter la;
    private Item item;


    @Override
    public void onStart() {
        super.onStart();

        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();

        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView)findViewById(R.id.listViewmain);

        la = new ListAdapter(this, R.layout.empty_view, m_parts);
        /*adapter = new ListAdapter<Item>(this,
                R.layout.empty_view, contnames);*/
        lv.setAdapter(la);

        EasyTracker.getInstance(this).activityStart(this);
        db = new DatabaseHandler(MainActivity.this);
        ct = new Contact();
      //  db.cleanup();


        String word=getIntent().getStringExtra(AppWidget.EXTRA_WORD);
        addContact = (Button) findViewById(R.id.addcontact);
        tv = (EditText) findViewById(R.id.contactnumber);
        tv2 = (EditText) findViewById(R.id.contactname);
        b = (Button) findViewById(R.id.pickcontact);
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_PICK,ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 100);
            }
        });
        addContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ct._name=tv2.getText().toString();
                ct._phone_number=tv.getText().toString();
                ImageView profile  = (ImageView)findViewById(R.id.contactImage);
                BitmapDrawable bitmapDrawable = ((BitmapDrawable) profile.getDrawable());
                Bitmap bitmap = bitmapDrawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] imageInByte = stream.toByteArray();
                ct._photo = imageInByte;

                db.Add_Contact(ct);
                la.refresh();

                sendTextToWidget(MainActivity.this);

            }
        });
        la.refresh();

      //  Toast.makeText(this, word, Toast.LENGTH_LONG).show();


    }

    private void sendTextToWidget(Context context) {
        Intent uiIntent = new Intent(ACTION_WIDGET_UPDATE_FROM_ACTIVITY);
        uiIntent.putExtra("test","Button clicked on Activity at ");
        context.sendBroadcast(uiIntent);
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            getContactData(data);
        }
    }

    public void getContactData(Intent data){

                    final EditText phoneInput = (EditText) findViewById(R.id.contactnumber);
                    final EditText contactInput = (EditText) findViewById(R.id.contactname);
                    Cursor cursor = null;
                    Cursor nameCursor = null;
                    String phoneNumber = "";
                    String contactName = "";
                    List<String> allNumbers = new ArrayList<String>();





                    int phoneIdx = 0;
                    int nameIdx = 0;
                    int imageIdx = 0;
                    int photoIdx = 0;
                    try {
                        Uri result = data.getData();
                        String id = result.getLastPathSegment();
                        String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.CommonDataKinds.Phone.NUMBER};
                        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[] { id }, null);
                        phoneIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
                        nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(id));
                        ImageView profile  = (ImageView)findViewById(R.id.contactImage);
                        InputStream photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),my_contact_Uri);

                        if ( photo_stream == null) {

                            profile.setImageResource(R.drawable.ic_launcher);
                        } else {
                            BufferedInputStream buf  = new BufferedInputStream(photo_stream);
                            Bitmap my_btmp = BitmapFactory.decodeStream(buf);
                            profile.setImageBitmap(my_btmp);
                        }


                        //imageIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.);
                        if (cursor.moveToFirst()) {
                            while (cursor.isAfterLast() == false) {
                                phoneNumber = cursor.getString(phoneIdx);
                                contactName = cursor.getString(nameIdx);

                                allNumbers.add(phoneNumber);
                                cursor.moveToNext();
                            }
                        } else {
                            //no results actions
                        }

                    } catch (Exception e) {
                        //error actions
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }

                        final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Choose a number");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                String selectedNumber = items[item].toString();
                                selectedNumber = selectedNumber.replace("-", "");
                                phoneInput.setText(selectedNumber);

                            }
                        });
                        AlertDialog alert = builder.create();
                        if(allNumbers.size() > 1) {
                            alert.show();
                        } else {
                            String selectedNumber = phoneNumber.toString();
                            selectedNumber = selectedNumber.replace("-", "");
                            phoneInput.setText(selectedNumber);
                        }
                        contactInput.setText(contactName);

                        ListView lv = (ListView)findViewById(R.id.contacts);
                    //    lv.addVit


                        if (phoneNumber.length() == 0) {
                            //no numbers found actions
                        } else
                        {

                        }

                    }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
