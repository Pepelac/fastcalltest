package com.PepelacStudio.fastcalltest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.PepelacStudio.fastcalltest.database.Contact;
import com.PepelacStudio.fastcalltest.database.DatabaseHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abantonx on 08.09.13.
 */
public class ListAdapter extends ArrayAdapter<Item> {
    public static String ACTION_WIDGET_UPDATE_FROM_ACTIVITY = "ACTION.WIDGET.UPDATE.FROM.ACTIVITY";
    private ArrayList<Item> objects;
    private DatabaseHandler db;
    Context context;
    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
        // TODO Auto-generated constructor stub
    }
    public ListAdapter(Context context, int textViewResourceId, ArrayList<Item> objects) {
        super(context, textViewResourceId, objects);
        this.objects = objects;
        this.context = context;

        db = new DatabaseHandler(context);
    }

    private class ViewHolder {
        Button button;
        TextView txtId;
        TextView txtDesc;
    }
    private List<Item> items;

    public ListAdapter(Context context, int resource, List<Item> items) {

        super(context, resource, items);

        this.items = items;

    }
    public void refresh()
    {

        this.clear();
        ArrayList<Contact> cont = db.Get_Contacts();
        ArrayList<Item> tests = new ArrayList<Item>() ;
        int t=0;
        for (Contact s : cont){

            //test[t]= s.getName();
            Item it = new Item (s.getName(), s.getID(), s.getPhoto());

            tests.add(it);
            t++;
        }
        if (cont.size()!=0){
           this.addAll(tests);
        }

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ViewHolder holder = null;
        Item i = objects.get(position);

        int IdfromText= -1;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.empty_view, null);
            holder = new ViewHolder();

            //holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.button= (Button) convertView.findViewById(R.id.buttonTest);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        final int t2 = IdfromText;
        final Item i2 = i;
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.Delete_Contact(i2.getId());
                notifyDataSetChanged();
                refresh();
                Intent uiIntent = new Intent(ACTION_WIDGET_UPDATE_FROM_ACTIVITY);
                uiIntent.putExtra("test","Button clicked on Activity at ");
                context.sendBroadcast(uiIntent);
            }
        });

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.empty_view, null);
        }



		/*
		 * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */


        if (i != null) {

            // This is how you obtain a reference to the TextViews.
            // These TextViews are created in the XML files we defined.

            TextView tt = (TextView) v.findViewById(R.id.testView);
            ImageView iv = (ImageView) v.findViewById(R.id.icon);



            // check to see if each individual textview is null.
            // if not, assign some text!
            if (tt != null){
                tt.setText(i.getName());
            }
            if (iv != null){
             //   Log.i("getPHoto", i.getPhoto().toString());

               // BitmapFactory.decodeByteArray(i.getPhoto(), 0, i.getPhoto().length);
                iv.setImageBitmap(BitmapFactory.decodeByteArray(i.getPhoto(), 0, i.getPhoto().length));
              //  iv.setImageResource(R.drawable.ic_launcher);
            }
            /*
            if (mtd != null){
                mtd.setText("$" + i.getPrice());
            }
            if (bt != null){
                bt.setText("Details: ");
            }
            if (btd != null){
                btd.setText(i.getDetails());
            }*/
        }


        // the view must be returned to our activity
        return v;

    }

}

