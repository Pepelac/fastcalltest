package com.PepelacStudio.fastcalltest;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by abantonx on 09.09.13.
 */

public class Item {
    private Uri photoUri;
    private String details;
    private String name;
    private int phone;
    private int id;
    private byte[] photo;

    public Item(){

    }

    public Item(String i, int id, byte[] photo){
        this.name = i;
        this.id = id;
        this.photo = photo;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] Photo) {
        this.photo = Photo;
    }

}
