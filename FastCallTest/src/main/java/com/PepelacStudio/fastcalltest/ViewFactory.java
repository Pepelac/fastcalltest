package com.PepelacStudio.fastcalltest;

/**
 * Created by abantonx on 03.09.13.
 */

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.PepelacStudio.fastcalltest.database.Contact;
import com.PepelacStudio.fastcalltest.database.DatabaseHandler;

import java.io.InputStream;
import java.util.ArrayList;

public class ViewFactory implements RemoteViewsService.RemoteViewsFactory {
    private  String[] items;
    private ArrayList<byte[]> itemsPgotos;
    public static String ACTION_WIDGET_UPDATE_POSITION= "ACTION.WIDGET.UPDATE.POSITION";
    private DatabaseHandler db;

   // private String[] items= new String[]{"test1","test2","test3","test4"};
    private Context ctxt=null;

    private int appWidgetId;

    public ViewFactory(Context ctxt, Intent intent) {
        db = new DatabaseHandler(ctxt);

        this.ctxt=ctxt;
        appWidgetId=intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }
    public void setItems(String[] string){
        items= string;
        itemsPgotos = new ArrayList<byte[]>();

    }



    @Override
    public void onCreate() {
        // no-op
    }

    @Override
    public void onDestroy() {
        // no-op
    }

    @Override
    public int getCount() {
        if (items == null) {
            return 0;
        }

        return(items.length);
    }

    @Override
    public RemoteViews getViewAt(int position) {

         RemoteViews row=new RemoteViews(ctxt.getPackageName(),
                R.layout.row);
        //if (position !=0) {
        //Log.i("position receive trans", Integer.toString(position));

        //ct = db.Get_Contact(position);
        row.setTextViewText(android.R.id.text1, items[position]);
        if (itemsPgotos.size() > 0) {
        row.setImageViewBitmap(android.R.id.icon, (BitmapFactory.decodeByteArray(itemsPgotos.get(position), 0, itemsPgotos.get(position).length)));
        }
        //row.setImageViewBitmap(android.R.id.icon1, (BitmapFactory.decodeByteArray(itemsPhoto[position], 0, itemsPhoto[position].length)));
        /*Uri testa = Uri.parse("content://com.android.contacts/contacts/227");
        row.setImageViewUri(android.R.id.icon, testa);
        row.setImageViewResource(android.R.id.icon, R.drawable.ic_launcher);*/
        Intent i = new Intent(this.ctxt, AppWidget.class);
        i.putExtra("position", position);
        row.setOnClickFillInIntent(android.R.id.text1, i);
        row.setOnClickFillInIntent(android.R.id.icon, i);



        return(row);
    }

    @Override
    public RemoteViews getLoadingView() {
        return(null);
    }

    @Override
    public int getViewTypeCount() {
        return(1);
    }

    @Override
    public long getItemId(int position) {
        return(position);
    }

    @Override
    public boolean hasStableIds() {
        return(true);
    }

    @Override
    public void onDataSetChanged() {
        ArrayList<Contact> cont = db.Get_Contacts();
        itemsPgotos.clear();
        String [] test =new String [cont.size()];
        int t = 0;
        if (cont.size()!=0){
        for (Contact s : cont){

            test[t]= s.getName();
            itemsPgotos.add(s.getPhoto());
            t++;
        items=test;
        }
        }
    }
}
