FastCallTest
============

<a href="https://play.google.com/store/apps/details?id=com.PepelacStudio.fastcalltest">
  <img alt="Get it on Google Play"
       src="https://developer.android.com/images/brand/en_generic_rgb_wo_60.png" />
</a>

Simple app that provides simple widget for home screen and lock screen.
Almost all people secure their phone by PIN/FaceUnlock/ etc...
And when you loose your phone no one will be able to call you to bring it back.
This app provides such functionality by adding several emergency numbers to your lock screen as a widget.
You can easily add contacts from your contact list.
